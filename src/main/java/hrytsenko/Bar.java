package hrytsenko;

import hrytsenko.api.Component;

@Component
public class Bar {

    public String execute() {
        return "bar";
    }

}
